package main

import "gitlab.com/codeprac/cms/internal/cli"

func main() {
	cli.GetCommand().Execute()
}
