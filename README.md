# Codepr.ac Content Management System

Content management system for Codeprac content. This powers the newsfeed dashboard and practice content.

- [Codepr.ac Content Management System](#codeprac-content-management-system)
- [Getting started](#getting-started)
  - [Required software](#required-software)
  - [Orientation and setup](#orientation-and-setup)
  - [Usage](#usage)
- [Documentation](#documentation)
- [License](#license)

# Getting started

This service exists to provide content for Codeprac's Newsfeed and Practice components and possibly more. It encapsulates the volatility present in content that a user will consume from Codeprac.

## Required software

| CLI invocation       | Description                                                                                                                        | Installation instructions                                         |
| -------------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
| `go`                 | At least 1.11 (for Go Modules support) for running/building this project                                                           | [🖇](https://golang.org/doc/install)                               |
| `protoc`             | For compiling Protobuf definitions                                                                                                 | [🖇](https://grpc.io/docs/protoc-installation/)                    |
| `protoc-gen-go`      | For generating Go code from Protobuf definitions                                                                                   | [🖇](https://grpc.io/docs/languages/go/quickstart/#prerequisites)  |
| `protoc-gen-go-grpc` | For generating gRPC code from Protobuf definitions                                                                                 | "                                                                 |
| `direnv`             | For running this project with a custom environment                                                                                 | [🖇](https://direnv.net/#getting-started)                          |
| `docker`             | For building + releasing the packaged image                                                                                        | [🖇](https://docs.docker.com/get-docker/)                          |
| `docker-commpose`    | For setting up the development environment, this is part of Docker Desktop but if you're using linux this is a manual installation | [🖇](https://docs.docker.com/compose/install/)                     |
| `grpcurl`            | For running experiments against the gRPC server                                                                                    | [🖇](https://github.com/fullstorydev/grpcurl/releases/)            |
| `kind`               | For running the deployment Helm chart locally                                                                                      | [🖇](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) |

## Orientation and setup

1. This project uses Docker to provision the environment
   - Run `make setup` to provision the environment with Docker Compose
   - Run `make teardown` when you're done to purge the Docker Compose environment
2. This project uses Go as the primary development language
   - Dependency installation is via Go Modules can be done using `make deps`
   - We try as much to follow the [Go Standard Layout](https://github.com/golang-standards/project-layout) as far as reasonably possible
3. This project uses MongoDB as the persistent data store
4. This project exposes both a REST (for gateways to proxy to) and a gRPC (for internal transactional messaging) interface
5. We use protobuf for the message protocol, run `make protobufs` to update the code generated from the `*.proto` files, this is done via use of `go:generate` in the `doc.go` of protobuf packages
6. The Main library used to:
   1. Handle the CLI implementation is [`spf13/cobra`](https://github.com/spf13/cobra)
   2. Handle routing is [`gorilla/mux`](https://github.com/gorilla/mux)
   3. Manage feed data is [`mmcdole/gofeed`](https://github.com/mmcdole/gofeed)

## Usage

1. For calling this service with gRPC, import from the `pkg` directory:
    ```go
    import "gitlab.com/codeprac/cms/pkg/cms"
    ```

# Documentation

1. [Configuration](./docs/configuration.md)
1. [Contributing](./docs/contributing.md)
1. [Deployment](./docs/deployment.md)
1. [Experimentation](./docs/experimentation.md)

# License

Code is licensed under The MIT License. [See full license](./LICENSE).
