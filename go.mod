module gitlab.com/codeprac/cms

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/mmcdole/gofeed v1.1.3
	github.com/prometheus/client_golang v0.9.3
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	github.com/usvc/go-config v0.4.1
	gitlab.com/codeprac/modules/go/log v0.1.0
	gitlab.com/codeprac/modules/go/server v0.10.0
	go.mongodb.org/mongo-driver v1.7.1
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v2 v2.4.0
)
