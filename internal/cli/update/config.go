package update

import (
	"github.com/usvc/go-config"
	databaseconfiguration "gitlab.com/codeprac/cms/internal/database/configuration"
	"gitlab.com/codeprac/modules/go/log"
)

const (
	KeyDataFile = "data-file"
)

var configSets = []config.Map{
	databaseconfiguration.Get(),
}

var conf = config.Map{
	KeyDataFile: &config.String{
		Default: "./data/feeds.yaml",
	},
}

func init() {
	for _, configSet := range configSets {
		for key, value := range configSet {
			if _, ok := conf[key]; ok {
				log.Warnf("configuration '%s' was already defined but has been overwritten", key)
			}
			conf[key] = value
		}
	}
}
