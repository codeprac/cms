package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/cms/internal/cli/start"
	"gitlab.com/codeprac/cms/internal/cli/update"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use: "cms",
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
	}
	command.AddCommand(update.GetCommand())
	command.AddCommand(start.GetCommand())
	return command
}
