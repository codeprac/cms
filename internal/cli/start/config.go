package start

import (
	"github.com/usvc/go-config"
	databaseconfiguration "gitlab.com/codeprac/cms/internal/database/configuration"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/codeprac/modules/go/server"
)

var configSets = []config.Map{
	databaseconfiguration.Get(),
	server.GetConfiguration(),
}

var conf = config.Map{}

func init() {
	for _, configSet := range configSets {
		for key, value := range configSet {
			if _, ok := conf[key]; ok {
				log.Warnf("configuration '%s' was already defined but has been overwritten", key)
			}
			conf[key] = value
		}
	}
}
