package start

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/cms/internal/api"
	"gitlab.com/codeprac/cms/internal/cli/utils"
	databaseconfiguration "gitlab.com/codeprac/cms/internal/database/configuration"
	"gitlab.com/codeprac/cms/internal/database/connection"
	"gitlab.com/codeprac/modules/go/log"
	"gitlab.com/codeprac/modules/go/server"
	"gitlab.com/codeprac/modules/go/server/middleware"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "start",
		Short: "Starts the content server",
		RunE: func(cmd *cobra.Command, args []string) error {

			// database
			connection, err := utils.ConnectToMongo(connection.MongoConnectionOpts{
				Hostnames:    conf.GetStringSlice(databaseconfiguration.KeyHostnames),
				Port:         conf.GetUint(databaseconfiguration.KeyPort),
				User:         conf.GetString(databaseconfiguration.KeyUsername),
				Password:     conf.GetString(databaseconfiguration.KeyPassword),
				Database:     conf.GetString(databaseconfiguration.KeyDatabase),
				AuthSource:   conf.GetString(databaseconfiguration.KeyAuthSource),
				IsTlsEnabled: conf.GetBool(databaseconfiguration.KeyUseTLS),
			})
			if err != nil {
				return fmt.Errorf("failed to connect to mongo: %s", err)
			}
			disconnectCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			defer connection.Disconnect(disconnectCtx)
			db := connection.Database(conf.GetString(databaseconfiguration.KeyDatabase))

			log.Infof("successfully connected to the databse")

			// grpc server

			log.Debug("setting up grpc server instance...")

			grpcServerOpts := server.NewGrpcServerOpts{
				Interface: conf.GetString(server.KeyInterface),
				Port:      uint16(conf.GetInt(server.KeyPort)) + 1,
			}
			grpcServerInstance := server.NewGrpcServer(grpcServerOpts)
			api.RegisterGrpcHandlers(api.GrpcHandlerOpts{
				Database: db,
				Instance: grpcServerInstance.Server,
			})
			grpcServerInstance.IsReflectionEnabled = true

			log.Info("successfully instantiated grpc server instance...")

			// http server

			log.Debugf("setting up http server instance...")

			httpServerOpts := server.NewHTTPServerOptions{
				Interface:      conf.GetString(server.KeyInterface),
				Port:           uint16(conf.GetInt(server.KeyPort)),
				AllowedMethods: conf.GetStringSlice(server.KeyCORSAllowedMethods),
				AllowedOrigins: conf.GetStringSlice(server.KeyCORSAllowedOrigins),
			}
			log.Debugf("allowed methods: ['%s']", strings.Join(httpServerOpts.AllowedMethods, "', '"))
			log.Debugf("allowed origins: ['%s']", strings.Join(httpServerOpts.AllowedOrigins, "', '"))
			httpServerInstance := server.NewHTTPServer(httpServerOpts)
			httpServerInstance.ErrorLogger = log.Errorf
			httpServerInstance.RequestLogger = log.Debugf

			log.Debugf("setting up http interface...")

			handler, err := api.GetHttpHandler(api.HttpHandlerOpts{Database: db})
			if err != nil {
				message := fmt.Sprintf("failed to initialise route handlers: %s", err)
				log.Error(message)
				return errors.New(message)
			}

			log.Debugf("setting up observability endpoints...")

			livenessProbe := server.NewHttpHealthcheckOpts{
				Path: "/healthz",
			}
			httpServerInstance.LivenessProbeHandler = server.NewHttpHealthcheck(livenessProbe)
			readinessProbe := server.NewHttpHealthcheckOpts{
				Path: "/readyz",
			}
			httpServerInstance.ReadinessProbeHandler = server.NewHttpHealthcheck(readinessProbe)
			metricsOpts := server.NewHttpMetricsPrometheusOpts{
				Path: "/metrics",
			}
			httpServerInstance.MetricsHandler = server.NewHttpMetricsPrometheus(metricsOpts)

			log.Debugf("setting up observability instrumentation for http endpoints...")

			endpointCallCount := *prometheus.NewCounterVec(prometheus.CounterOpts{
				Name: "http_requests_total",
				Help: "Number of requests",
			}, []string{"method", "path", "code"})
			prometheus.Register(endpointCallCount)
			httpServerInstance.MetricsRecorder = func(m middleware.Metrics) {
				endpointCallCount.WithLabelValues(m.RequestMethod, m.RequestPath, strconv.Itoa(m.ResponseCode)).Add(1)
			}

			// start

			var processManager sync.WaitGroup
			exitErrors := []string{}

			processManager.Add(1)
			go func() {
				log.Infof("starting the http server on '%s:%v'...", httpServerOpts.Interface, httpServerOpts.Port)
				if err := httpServerInstance.Start(handler); err != nil {
					exitErrors = append(exitErrors, err.Error())
				}
				processManager.Done()
			}()
			processManager.Add(1)
			go func() {
				log.Infof("starting the grpc server on '%s'...", grpcServerInstance.Address)
				if err := grpcServerInstance.Start(); err != nil {
					exitErrors = append(exitErrors, err.Error())
				}
				processManager.Done()
			}()

			processManager.Wait()
			if len(exitErrors) > 0 {
				return fmt.Errorf("failed to exit cleanly: ['%s']", strings.Join(exitErrors, "', '"))
			}
			return nil
		},
	}
	conf.ApplyToCobra(&command)
	return &command
}
