package cms

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type QueryOpts struct {
	Offset        int64
	Limit         int64
	SortKey       string
	SortDirection int
}

// bulkWrite standardises the bulk write operation that happens in all
// the upsert operations
func bulkWrite(what []mongo.WriteModel, to *mongo.Collection) error {
	bulk := options.BulkWriteOptions{}
	bulk.SetOrdered(true)
	_, err := to.BulkWrite(context.Background(), what, &bulk)
	return err
}

// getIdsOpts provides a structure for the parameters for `getIds`
type getIdsOpts struct {
	CollectionName string
	Database       *mongo.Database
	Keys           []string
	SearchKey      string
}

// getIds retrieves the mongo ObjectIDs for items in the collection `opts.CollectionName`
// by selecting for when the search key `opts.SearchKey` equals the value `opts.Keys`
func getIds(opts getIdsOpts) ([]string, error) {
	result, err := opts.Database.Collection(opts.CollectionName).Find(context.Background(), bson.M{
		opts.SearchKey: bson.M{"$in": opts.Keys},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to find %s: %s", opts.CollectionName, err)
	}
	ids := []string{}
	var results []bson.M
	result.All(context.Background(), &results)
	for _, r := range results {
		if objectID, ok := r["_id"].(primitive.ObjectID); ok {
			ids = append(ids, objectID.Hex())
		}
	}
	return ids, nil
}

// upsertable defines a structure for the parameters for `newUpsertOperation`
type upsertable interface {
	GetFilterDocument() bson.M
	GetKey() string
	GetUpdateDocument() bson.M
}

// newUpsertOperation standardises creation of a upsert operation for eventual
// use in `bulkWrite`
func newUpsertOperation(on upsertable) *mongo.UpdateOneModel {
	operation := mongo.NewUpdateOneModel()
	operation.SetFilter(on.GetFilterDocument())
	operation.SetUpsert(true)
	operation.SetUpdate(on.GetUpdateDocument())
	return operation
}

func toStringPointer(str string) *string {
	return &str
}
