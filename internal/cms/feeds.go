package cms

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type GetFeedsOpts struct {
	Database *mongo.Database
	FeedIDs  []string
	QueryOpts
}

func GetFeeds(opts GetFeedsOpts) (Feeds, error) {
	collection := opts.Database.Collection(CollectionFeeds)
	findContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{}
	if opts.FeedIDs != nil && len(opts.FeedIDs) > 0 {
		filter[FeedSearchKey] = bson.M{
			"$in": opts.FeedIDs,
		}
	}

	find := options.Find()
	if opts.QueryOpts.SortKey != "" && opts.QueryOpts.SortDirection != 0 {
		find.SetSort(bson.M{opts.QueryOpts.SortKey: opts.QueryOpts.SortDirection})
	}
	limit := DefaultFeedLimit
	if opts.QueryOpts.Limit > 0 {
		limit = opts.QueryOpts.Limit
	}
	find.SetLimit(limit)
	find.SetSkip(opts.QueryOpts.Offset)

	result, err := collection.Find(findContext, filter, find)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve feeds from database: %s", err)
	}
	feeds := Feeds{}
	if err := result.All(context.Background(), &feeds); err != nil {
		return nil, fmt.Errorf("failed to parse feeds list: %s", err)
	}
	return feeds, nil
}

type Feeds []Feed

func (feeds Feeds) ToProtobuf() []*pb.Feed {
	pbFeeds := []*pb.Feed{}
	for _, feed := range feeds {
		pbFeeds = append(pbFeeds, feed.ToProtobuf())
	}
	return pbFeeds
}

// Upsert orchestrates the upsert operations of this list of Feed instances
// into the database and returns a list of their database object IDs
func (feeds Feeds) Upsert(db *mongo.Database) ([]string, error) {
	if len(feeds) == 0 {
		return []string{}, nil
	}
	operations := []mongo.WriteModel{}
	targetFeeds := []string{}
	for _, feed := range feeds {
		var err error
		if feed.CategoryIDs, err = feed.Categories.Upsert(db); err != nil {
			return nil, fmt.Errorf("failed to update categories for feed '%s': %s", feed.Link, err)
		}
		if feed.AuthorIDs, err = feed.Authors.Upsert(db, CollectionAuthors); err != nil {
			return nil, fmt.Errorf("failed to update authors for feed '%s': %s", feed.Link, err)
		}
		operations = append(operations, newUpsertOperation(feed))
		targetFeeds = append(targetFeeds, feed.GetKey())
	}
	if err := bulkWrite(operations, db.Collection(CollectionFeeds)); err != nil {
		return nil, fmt.Errorf("failed to upsert feeds: %s", err)
	}
	return getIds(getIdsOpts{
		CollectionName: CollectionFeeds,
		Database:       db,
		Keys:           targetFeeds,
		SearchKey:      FeedSearchKey,
	})
}
