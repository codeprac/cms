package cms

import (
	"testing"

	"github.com/stretchr/testify/suite"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

type CategoryTests struct {
	suite.Suite
	isUpsertable upsertable
}

func TestCategory(t *testing.T) {
	suite.Run(t, new(CategoryTests))
}

// Test__InterfaceConstraints will prevent tests from running if `Category`
// no longer implements `upsertable`
func (s CategoryTests) Test__InterfaceConstraints() {
	s.isUpsertable = Category{}
}

func (s CategoryTests) Test_NewCategoryFromProtobuf() {
	pbCategory := &pb.Category{
		Id:       1,
		ObjectID: "object_id",
		Uuid:     "uuid",
		Name:     "name",
	}
	category := NewCategoryFromProtobuf(pbCategory)
	s.Equal(category.ID, 1)
	s.Equal(category.ObjectID, "object_id")
	s.Equal(category.UUID, "uuid")
	s.Equal(category.Name, "name")
}

func (s CategoryTests) Test_Category_GetKey() {
	category := Category{Name: "NamE"}
	s.Equal(category.GetKey(), "name")
}

func (s CategoryTests) Test_Category_FilterAndUpdateMatches() {
	category := Category{Name: "NamE"}
	filterDocument := category.GetFilterDocument()
	updateDocument := category.GetUpdateDocument()
	updateDocumentSetter := updateDocument["$set"]
	s.Equal(updateDocumentSetter, filterDocument)
}

func (s CategoryTests) Test_Category_GetUpdateDocument() {
	category := Category{Name: "NamE"}
	updateDocumentSetter := category.GetUpdateDocument()["$set"].(bson.M)
	s.Contains(updateDocumentSetter, "name")
}

func (s CategoryTests) Test_Category_ToProtobuf() {
	category := Category{ObjectID: "objectID", Name: "NaME"}
	pbCategory := category.ToProtobuf()
	s.Equal(category.ObjectID, pbCategory.ObjectID)
	s.Equal(category.Name, pbCategory.Name)
}
