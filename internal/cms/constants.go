package cms

const (
	CollectionAuthors          = "authors"
	CollectionCategories       = "categories"
	CollectionFeeds            = "feeds"
	CollectionItems            = "items"
	DefaultAuthorLimit   int64 = 50
	DefaultCategoryLimit int64 = 50
	DefaultFeedLimit     int64 = 30
	DefaultItemLimit     int64 = 15
	TimestampFormatFeeds       = "Mon, 02 Jan 2006 15:04:05 -0700"
)
