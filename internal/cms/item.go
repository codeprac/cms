package cms

import (
	"strings"
	"time"

	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	ItemSearchKey = "guid"
)

type Item struct {
	ID          int        `json:"-"`
	ObjectID    string     `json:"id" bson:"_id"`
	UUID        string     `json:"uuid,omitempty"`
	GUID        string     `json:"guid"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	ImageURL    string     `json:"imageURL"`
	Content     string     `json:"content"`
	Link        string     `json:"link"`
	Updated     time.Time  `json:"updated"`
	Published   time.Time  `json:"published"`
	Categories  Categories `json:"categories" bson:"-"`
	CategoryIDs []string   `json:"categoryIDs" bson:"categories"`
	Authors     People     `json:"authors" bson:"-"`
	AuthorIDs   []string   `json:"authorIDs" bson:"authors"`
}

func (item Item) GetFilterDocument() bson.M {
	return bson.M{ItemSearchKey: item.GetKey()}
}

func (item Item) GetKey() string {
	return strings.Trim(strings.ToLower(item.GUID), "/ ")
}

func (item Item) GetUpdateDocument() bson.M {
	return bson.M{"$set": bson.M{
		ItemSearchKey: item.GetKey(),
		"link":        item.Link,
		"title":       item.Title,
		"description": item.Description,
		"imageURL":    item.ImageURL,
		"content":     item.Content,
		"updated":     item.Updated,
		"published":   item.Published,
		"authors":     item.AuthorIDs,
		"categories":  item.CategoryIDs,
	}}
}

func (item Item) ToProtobuf() *pb.Item {
	return &pb.Item{
		ObjectID:    item.ObjectID,
		Guid:        item.GUID,
		Title:       item.Title,
		Description: item.Description,
		ImageURL:    item.ImageURL,
		Link:        item.Link,
		Updated:     item.Updated.Format(TimestampFormatFeeds),
		Published:   item.Published.Format(TimestampFormatFeeds),
		CategoryIDs: item.CategoryIDs,
		AuthorIDs:   item.AuthorIDs,
	}
}
