package cms

import (
	"context"
	"fmt"
	"time"

	"github.com/mmcdole/gofeed"
	pb "gitlab.com/codeprac/cms/pkg/cms"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewItemsFromGofeed(is []*gofeed.Item) Items {
	items := Items{}
	for _, i := range is {
		item := Item{
			GUID:        i.GUID,
			Title:       i.Title,
			Description: i.Description,
			Content:     i.Content,
			Link:        i.Link,
			Categories:  NewCategoriesFromString(i.Categories),
			Authors:     NewPeopleFromGofeed(i.Authors),
		}
		if i.Image != nil {
			item.ImageURL = i.Image.URL
		}
		if i.UpdatedParsed != nil {
			item.Updated = *i.UpdatedParsed
		}
		if i.PublishedParsed != nil {
			item.Published = *i.PublishedParsed
		}
		items = append(items, item)
	}
	return items
}

type GetItemsOpts struct {
	Database *mongo.Database
	ItemIDs  []string
	QueryOpts
}

func GetItems(opts GetItemsOpts) (Items, error) {
	collection := opts.Database.Collection(CollectionItems)
	findContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{}
	if opts.ItemIDs != nil && len(opts.ItemIDs) > 0 {
		filter[ItemSearchKey] = bson.M{
			"$in": opts.ItemIDs,
		}
	}

	find := options.Find()
	if opts.QueryOpts.SortKey != "" && opts.QueryOpts.SortDirection != 0 {
		find.SetSort(bson.M{opts.QueryOpts.SortKey: opts.QueryOpts.SortDirection})
	}
	limit := DefaultItemLimit
	if opts.QueryOpts.Limit > 0 {
		limit = opts.QueryOpts.Limit
	}
	find.SetLimit(limit)
	find.SetSkip(opts.QueryOpts.Offset)

	result, err := collection.Find(findContext, filter, find)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve items from database: %s", err)
	}
	items := Items{}
	if err := result.All(context.Background(), &items); err != nil {
		return nil, fmt.Errorf("failed to parse items list: %s", err)
	}
	return items, nil
}

type Items []Item

func (items Items) ToProtobuf() []*pb.Item {
	pbItems := []*pb.Item{}
	for _, item := range items {
		pbItems = append(pbItems, item.ToProtobuf())
	}
	return pbItems
}

// Upsert orchestrates the upsert operations of this list of Item instances
// into the database and returns a list of their database object IDs
func (items Items) Upsert(db *mongo.Database) ([]string, error) {
	if len(items) == 0 {
		return []string{}, nil
	}
	operations := []mongo.WriteModel{}
	targetItems := []string{}
	for _, item := range items {
		var err error
		if item.CategoryIDs, err = item.Categories.Upsert(db); err != nil {
			return nil, fmt.Errorf("failed to update categories for item '%s': %s", item.Link, err)
		}
		if item.AuthorIDs, err = item.Authors.Upsert(db, CollectionAuthors); err != nil {
			return nil, fmt.Errorf("failed to update authors for item '%s': %s", item.Link, err)
		}
		operations = append(operations, newUpsertOperation(item))
		targetItems = append(targetItems, item.GetKey())
	}
	if err := bulkWrite(operations, db.Collection(CollectionItems)); err != nil {
		return nil, fmt.Errorf("failed to upsert items: %s", err)
	}
	return getIds(getIdsOpts{
		CollectionName: CollectionItems,
		Database:       db,
		Keys:           targetItems,
		SearchKey:      ItemSearchKey,
	})
}
