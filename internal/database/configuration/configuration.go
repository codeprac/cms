package configuration

import "github.com/usvc/go-config"

const (
	KeyHostnames  = "db-hostnames"
	KeyPort       = "db-port"
	KeyUsername   = "db-username"
	KeyPassword   = "db-password"
	KeyDatabase   = "db-database"
	KeyAuthSource = "db-auth-source"
	KeyUseTLS     = "db-use-tls"

	DefaultHostname   = "localhost"
	DefaultPort       = 27017
	DefaultUsername   = "user"
	DefaultPassword   = "password"
	DefaultDatabase   = "content"
	DefaultAuthSource = "admin"
	DefaultTLS        = false
)

func Get() config.Map {
	return config.Map{
		KeyHostnames: &config.StringSlice{
			Default: []string{DefaultHostname},
		},
		KeyPort: &config.Uint{
			Default: DefaultPort,
		},
		KeyUsername: &config.String{
			Default: DefaultUsername,
		},
		KeyPassword: &config.String{
			Default: DefaultPassword,
		},
		KeyDatabase: &config.String{
			Default: DefaultDatabase,
		},
		KeyAuthSource: &config.String{
			Default: DefaultAuthSource,
		},
		KeyUseTLS: &config.Bool{
			Default: DefaultTLS,
		},
	}
}
