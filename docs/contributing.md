# Contributing

This document covers aspects of working on this repository.

- [Contributing](#contributing)
  - [Updating protobufs](#updating-protobufs)
  - [Testing code](#testing-code)
    - [Automated tests](#automated-tests)
  - [Releasing code](#releasing-code)

## Updating protobufs

The `go:generate` tag is added into the `doc.go` documentation file to automatically generate Protobuf definitions for the package. To automatically generate all definitions, run:

```sh
make protobufs;

# or if you have trust issues with packaged commands:
go generate ./pkg/...;
```

## Testing code

### Automated tests

Before commiting code, you can run the following:

1. `make lint` to perform static analysis on the Go code
2. `make test` to run all unit tests

## Releasing code

Code is released using Docker images. You can run the following:

1. `make image` to package the code into a Docker image
2. `make release` to publish the Docker image

When running either of the above without repository management privileges, you'll need to set the `image_cms_url` property to your own. You may do this by running:

```sh
make image image_cms_url=codeprac/cms;
make release image_cms_url=codeprac/cms;
```

Alternatively, create a `Makefile.properties` file and enter in as its contents:

```make
image_cms_url := codeprac/cms
```

And then invoke the Make recipe without arguments

[Back to main README.md](../README.md).
