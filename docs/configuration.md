# Configuration

This document covers environment variables and other ways to configure this service

- [Configuration](#configuration)
  - [Environment configuration](#environment-configuration)

## Environment configuration

| Key            | Default                 | Description                                                                                                          |
| -------------- | ----------------------- | -------------------------------------------------------------------------------------------------------------------- |
| `DB_HOSTNAMES` | `[]string{"localhost"}` | Comma-delimited hostnames for the database connection, usually only specify one unless Mongo is running in a cluster |
| `DB_PORT`      | `27017`                 | Port the database connection should use                                                                              |
| `DB_USERNAME`  | `"user"`                | User to use to authenticate with the database                                                                        |
| `DB_PASSWORD`  | `"password"`            | Password to use to authenticate with the database                                                                    |
| `DB_DATABASE`  | `"content"`             | Name of the database                                                                                                 |
| `DB_USE_TLS`   | `false`                 | Indicates whether TLS should be used                                                                                 |

[Back to main README.md](../README.md).
