image_cms_registry := registry.gitlab.com
image_cms_owner := codeprac
image_cms_path := cms
cmd_name := cms

-include ./Makefile.properties

image_cms_url ?= $(image_cms_registry)/$(image_cms_owner)/$(image_cms_path)
cms_release_tag ?= latest

# init
setup:
	docker-compose --file ./docker-compose.yml up -d --remove-orphans
teardown:
	docker-compose --file ./docker-compose.yml down
# develop
deps:
	go mod vendor
protobufs:
	go generate ./pkg/...
start:
	go run ./cmd/cms start
lint:
	go vet ./...
test:
	mkdir -p ./coverage
	go test -v -mod=mod -covermode=atomic -coverpkg=./... -coverprofile=./coverage/cms.out ./...
build:
	CGO_ENABLED=0 go build \
		-ldflags "-extldflags 'static' -s -w" \
		-o ./bin/$(cmd_name)_$$(go env GOOS)_$$(go env GOARCH) \
		./cmd/$(cmd_name)
	cd ./bin && sha256sum $(cmd_name)_$$(go env GOOS)_$$(go env GOARCH) > $(cmd_name)_$$(go env GOOS)_$$(go env GOARCH).sha256
image:
	docker build \
		--file ./Dockerfile \
		--build-arg VERSION=$(cms_release_tag) \
		--tag $(image_cms_url):latest \
		--target final \
		.
release: image
	docker tag $(image_cms_url):latest $(image_cms_url):$(cms_release_tag)
	docker push $(image_cms_url):$(cms_release_tag)
